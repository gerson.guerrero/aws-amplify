import { useEffect, useState } from 'react'
import { API, graphqlOperation } from 'aws-amplify'
import { createTodo, deleteTodo } from './graphql/mutations'
import { listTodos } from './graphql/queries'
import {
  Alert,
  Badge,
  Button,
  Card,
  Heading,
  TextField,
  Loader,
  Text,
  withAuthenticator
} from '@aws-amplify/ui-react'
import './App.css'

const initialState = { name: '', description: '' }

const App = ({ signOut, user }) => {
  const [formState, setFormState] = useState(initialState)
  const [loading, setLoading] = useState(false)
  const [todos, setTodos] = useState([])
  const [error, setError] = useState(null)

  useEffect(() => {
    fetchTodos()
  }, [])

  const setInput = (key, value) => {
    setFormState({ ...formState, [key]: value })
  }

  const fetchTodos = async () => {
    try {
      setLoading(true)
      const todoData = await API.graphql(graphqlOperation(listTodos))
      const todos = todoData.data.listTodos.items
      setTodos(todos)
    } catch (error) {
      console.log('error fetching todos...')
    } finally {
      setLoading(false)
    }
  }

  const addTodo = async () => {
    try {
      if (!formState.name || !formState.description) return
      setLoading(true)
      const todo = { ...formState }
      const resp = await API.graphql(
        graphqlOperation(createTodo, { input: todo })
      )
      setTodos([...todos, resp?.data?.createTodo])
      setFormState(initialState)
    } catch (error) {
      console.log('error creating todo:', error)
    } finally {
      setLoading(false)
    }
  }

  const removeTodo = async (id) => {
    try {
      setLoading(true)
      await API.graphql(graphqlOperation(deleteTodo, { input: { id } }))
      setTodos(todos.filter((todo) => todo.id !== id))
    } catch (error) {
      error?.errors.map((err) => setError(err.message))
    } finally {
      setLoading(false)
    }
  }

  return (
    <main className="container">
      {error ? (
        <Alert
          heading="Error"
          isDismissible={true}
          onDismiss={() => setError(null)}
          variation="error"
        >
          {error}
        </Alert>
      ) : (
        <></>
      )}
      <header className='header'>
        <h1>📚 Amplify Todos</h1>
        <div>
          <Badge variation="success">
          { user?.username }
          </Badge>
          <Button variation='link' size='small' onClick={signOut}>Sign out</Button>
        </div>
      </header>
      <section>
        <TextField
          placeholder="Name"
          onChange={(event) => setInput('name', event.target.value)}
          value={formState.name}
        />
        <TextField
          placeholder="Description"
          onChange={(event) => setInput('description', event.target.value)}
          value={formState.description}
        />
        <Button loadingText='⌛' isLoading={loading} margin="10px 0" variation="primary" onClick={addTodo}>
          Create todo
        </Button>
      </section>
      {loading ? (
        <Loader variation="linear" />
      ) : (
        <section className="todos">
          {todos.map((todo, index) => (
            <Card
              className="todo"
              variation="elevated"
              key={todo.id ? todo.id : index}
            >
              <Heading margin="2px 0 10px 0" level={5}>
                {todo.name}
              </Heading>
              <Text as="span">{todo.description}</Text>
              <Button
                margin="5px 0"
                color="red"
                size="small"
                isFullWidth={true}
                onClick={() => removeTodo(todo.id)}
              >
                Complete/ Cancel
              </Button>
            </Card>
          ))}
        </section>
      )}
    </main>
  )
}

export default withAuthenticator(App)
